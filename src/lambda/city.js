exports async function handler(event, context) {
    var datan = [{"城市":"台北市","市長":"柯文哲"},{"城市":"新北市","市長":"侯友宜"}, {"城市":"桃園市","市長":"鄭文燦"}, {"城市":"臺中市","市長":"盧秀燕"}, {"城市":"臺南市","市長":"黃偉哲"}];
    var prefix = "直轄市";
    return {
        statusCode: 200,
        body: return datan.map((item) => '<li>' + prefix + item + '</li>')
    };
};